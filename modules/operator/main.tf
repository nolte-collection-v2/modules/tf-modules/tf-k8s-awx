data "http" "opreator" {
  url = "https://raw.githubusercontent.com/ansible/awx-operator/devel/deploy/awx-operator.yaml"
}

locals {
  operator_manifests = split("---", data.http.opreator.body)
}
resource "kubectl_manifest" "operator" {
  count     = length(local.operator_manifests) - 1
  yaml_body = local.operator_manifests[count.index + 1]
}
