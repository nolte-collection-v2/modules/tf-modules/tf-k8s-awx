module "operator" {
  source = "../operator"
}

# give Certsmanger Time to Work
resource "time_sleep" "wait_awx_operator" {
  depends_on = [module.operator]

  create_duration = "30s"
}

module "install" {
  depends_on        = [time_sleep.wait_awx_operator]
  source            = "../install"
  namespace         = var.namespace
  ingress_domain    = var.ingress_domain
  tower_credentials = var.tower_credentials
}
