variable "namespace" {

}

variable "ingress_domain" {

}

variable "tower_credentials" {
  type = object({
    username                   = string
    email                      = string
    admin_password             = string
    broadcast_websocket_secret = string
  })
  default = {
    username                   = "test"
    email                      = "test@example.com"
    admin_password             = "changeme"
    broadcast_websocket_secret = "changeme"
  }
}
