


resource "kubectl_manifest" "operator" {
  yaml_body = <<YAML
apiVersion: awx.ansible.com/v1beta1
kind: AWX
metadata:
  name: awx
  namespace: ${var.namespace}
spec:
  deployment_type: awx
  tower_admin_user: ${var.tower_credentials.username}
  tower_admin_email: ${var.tower_credentials.email}
  tower_admin_password: ${var.tower_credentials.admin_password}
  tower_broadcast_websocket_secret: ${var.tower_credentials.broadcast_websocket_secret}
  tower_ingress_type: Ingress
  tower_hostname: awx.${var.ingress_domain}
YAML
}
