
resource "kubernetes_namespace" "awx" {
  metadata {
    name = "acc-awx"
  }
}

module "operator" {
  source         = "../../modules/wrapper"
  namespace      = kubernetes_namespace.awx.metadata[0].name
  ingress_domain = "acc-awx.k8s.private"
}
