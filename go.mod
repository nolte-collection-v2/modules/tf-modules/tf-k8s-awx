module github.com/nolte/tf-k8s-awx

go 1.14

require (
	github.com/gruntwork-io/terratest v0.30.3
	github.com/magefile/mage v1.10.0
	github.com/nolte/plumbing v0.0.2-0.20200820161834-674f1ee627a1
	github.com/stretchr/testify v1.6.1
)
